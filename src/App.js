import React, { Component } from 'react';
import TargetSelector from './targetSelectorComponent';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      results: '',
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.results !== this.state.results && this.codeRef) {
      window.hljs.highlightBlock(this.codeRef);
    }
  }

  render() {
    return (
      <main className="container">

        <div className="jumbotron mt-5">
          <h1 className="display-5">Find your local representatives</h1>
          <p className="lead">React component used to try out the Representatives service.</p>
        </div>

        <TargetSelector
          onRequest={() => this.setState({ results: '' })}
          onError={() => this.setState({ results: <span className="badge badge-warning">network error</span> })}
          onReceive={results => {
            const code = JSON.stringify(results, null, '\t');
            this.setState({ results: (
              <pre className="card-text">
                <code ref={_ => {this.codeRef = _}} className="json">{code}</code>
              </pre>
            )});
          }}
        />

        {this.state.results &&
          <div className="card mt-5">
            <div className="card-header">Results</div>
            <div className="card-body">
              {this.state.results}
            </div>
          </div>
        }

      </main>
    );
  }


}

export default App;
