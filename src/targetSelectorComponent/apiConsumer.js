const baseUrl = '/api/representatives';

export default function apiConsumer({ onError, onRequest, onReceive }) {
  return values => {
    if (!values.address) return;

    const params = Object.entries(values)
      .map(([key,value]) => `${key}=${encodeURIComponent(value)}`)
      .join('&');

    onRequest(values);

    fetch(`${baseUrl}?${params}`)
      .then(response => {
        if (response.ok) return response.json();
        throw new Error("Oops, there has been an error with the request.");
      })
      .then(json => onReceive(json))
      .catch(err => onError(err));
  }
}