import React from 'react';
import PropTypes from 'prop-types';
import TargetSelectorForm from './reactComponents';
import apiConsumer from './apiConsumer';

export default function TargetSelector({ onError, onRequest, onReceive }) {
  return <TargetSelectorForm
    onError={onError}
    onRequest={onRequest}
    onReceive={onReceive}
    apiConsumer={apiConsumer}
  />;
}

TargetSelector.propTypes = {
  onError: PropTypes.func,
  onRequest: PropTypes.func,
  onReceive: PropTypes.func,
};
