import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class TargetSelectorForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fetching: false,
      representativeType: '',
      address: '',
    };

    this.fetch = props.apiConsumer({
      onError: this.whenError.bind(this),
      onReceive: this.whenReceive.bind(this),
      onRequest: this.whenRequest.bind(this),
    });

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  whenError(err) {
    this.setState({ fetching: false });

    const { onError } = this.props;
    if (typeof onError === 'function') onError(err);
  }

  whenRequest(values) {
    this.setState({ fetching: true });

    const { onRequest } = this.props;
    if (typeof onRequest === 'function') onRequest(values);
  }

  whenReceive(results) {
    this.setState({ fetching: false });

    const { onReceive } = this.props;
    if (typeof onReceive === 'function') onReceive(results);
  }

  handleChange({ target: { name, value } }) {
    this.setState({ [name]: value });
  }

  handleSubmit(event) {
    event.preventDefault();

    const { address, representativeType } = this.state;
    this.fetch({ address, representativeType });
  }

  render() {
    const { fetching } = this.state;
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="form-row">
          <div className="form-group col-5">
            <label htmlFor="representativeType">Representative mandate:</label>
            <select
              name="representativeType"
              className="form-control form-control-lg"
              id="representativeType"
              value={this.state.representativeType}
              onChange={this.handleChange}
            >
              <option />
              <option value="deputy">Deputy</option>
              <option value="mayor">Mayor</option>
              <option value="regionalCouncil">Regional Council</option>
              <option value="departmentalCouncil">Departmental Council</option>
            </select>
          </div>
          <div className="form-group col-7">
            <label htmlFor="address">Address:</label>
            <input
              required
              name="address"
              id="address"
              className="form-control form-control-lg"
              type="text"
              value={this.state.address}
              placeholder="Enter your address"
              onChange={this.handleChange}
            />
          </div>
        </div>
        <input
          className="btn btn-lg btn-block btn-primary"
          type="submit"
          value={fetching ? 'loading...' : 'Search'}
          disabled={fetching}
          aria-disabled={fetching}
        />
      </form>
    );
  }
}

TargetSelectorForm.propTypes = {
  apiConsumer: PropTypes.func,
  onError: PropTypes.func,
  onRequest: PropTypes.func,
  onReceive: PropTypes.func,
};